<nav class="top-nav">
    <ul>
        <li>
            <a href="<?php echo URLROOT; ?>/index">Główna strona</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/about">O nas</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/projects">Projekty</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/posts">Blog</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/contact">Kontakt</a>
        </li>
        <li class="btn-login">
            <?php if (isset($_SESSION['user_id'])) : ?>
                <a href="<?php echo URLROOT; ?>/users/logout">Wyloguj</a>
            <?php else : ?>
                <a href="<?php echo URLROOT; ?>/users/login">Zaloguj</a>
            <?php endif; ?>
        </li>
    </ul>
</nav>