<?php
require APPROOT . '/views/includes/head.php';
?>

<div class="navbar">
    <?php
    require APPROOT . '/views/includes/navigation.php';
    ?>
</div>

<div class="container-login">
    <div class="wrapper-login">
        <h2>Panel Logowania</h2>

        <form action="<?php echo URLROOT; ?>/users/login" method="POST">
            <input type="text" placeholder="Nazwa użytkownika *" name="username">
            <span class="invalidFeedback">
                <?php echo $data['usernameError']; ?>
            </span>

            <input type="password" placeholder="Hasło *" name="password">
            <span class="invalidFeedback">
                <?php echo $data['passwordError']; ?>
            </span>

            <button id="submit" type="submit" value="submit">Zaloguj</button>

            <p class="options">Nie masz konta? <a href="<?php echo URLROOT; ?>/users/register">Zarejestruj się!</a></p>
        </form>
    </div>
</div>